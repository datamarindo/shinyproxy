## comandos usados con docker

`docker ps` 		 enlista los contenedores que estan corriendo
`docker images` 		 enlista todas las imágenes
`-d` 			# lo detacha o demoniza
`-i` 			# interactivo, aún como demonio
`-t`			# tty

## para crear imagen

docker build -t openanalytics/shinyproxy-template . 		# -t Name and optionally a tag (format: "name:tag")

## Dios bendiga a los tutoriales de digitalocean

https://www.digitalocean.com/community/tutorials/how-to-share-data-between-the-docker-container-and-the-host

## PARA CORRERLA EN DOCKER

`name=`  le da un nombre con el que la podemos borrar
`-v` ~/data:/root/data nos monta en un directorio de la compu el respectivo directorio del contenedor

`sudo docker run --name=appelio2 -it -p 3838:3838  -v ~/data:/root/data openanalytics/shinyproxy-template`

En el application.yml de shinyproxy el container-volume no acepta "~" así que se pone como absoluta

```yml
 - id: shinyproxy-template
    container-image: openanalytics/shinyproxy-template
    display-name: Aplicacion de facturacion
    access-groups: scientists
    container-volumes: ["/home/datamarindo/Documents:/root/data"]
    logo-url: "file:///home/datamarindo/foto.png"
```
