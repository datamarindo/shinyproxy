#!/usr/bin/env python3

# en el crontab se escribe toda la ruta con el ./ para que ejecute
# 0 * * * * /home/datamarindo/Documents/prueba_facs/./uuid_facturas.py
# el glob necesitará la ruta completa, así como el df.to_csv

import glob
import pandas as pd
from xml.etree.ElementTree import ElementTree
tree = ElementTree()
archis = [glob.glob("/home/datamarindo/Documents/prueba_facs/**/*.%s" % colita , recursive = True) for colita in ["XML", "xml"]]

archis = sum(archis,[])
uuids = [tree.parse(root)[4][0].attrib["UUID"] for root in archis]
rfcs = [tree.parse(root)[0].attrib["Rfc"] for root in archis]
fecha = [tree.parse(root).attrib["Fecha"] for root in archis]

dictionary = {"uuids": uuids, "rfcs": rfcs, "fecha": fecha}

uuiddf = pd.DataFrame(dictionary)
uuiddf.to_csv("/home/datamarindo/Documents/prueba_facs/UUIDS.csv")


