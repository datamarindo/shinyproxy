library(shiny)

# TODO que saque la tabla con los datos de los archivos subidos
# TODO 
ruta =  "/root/data"
userName <-  Sys.getenv("SHINYPROXY_USERNAME")
munis = read.csv("munis_estados_wise.csv")

server = shinyServer(function(input, output, session){
  observeEvent(req(input$confirmacion, input$botones != 0, input$myFile, input$myFile2, input$myFile3), 
               { output$otroscontroles = 
                    renderUI({
                      tagList(actionButton("subir", "Enviar archivos",  class = "btn-lg btn-success",
                                                       style='padding:7px; font-size:95%'))
                      }) }, 
                  ignoreInit = T, ignoreNULL = T) 
  observe({
    updateSelectizeInput(session, "munis", choices = as.character(munis[munis$NOM_ENT %in% input$state,"NOMGEO"]))
  })
  observeEvent(input$subir, {
     directorio = paste0(ruta, "/", input$botones, "/", 
          iconv(tolower(userName),from = "utf8", to = "ASCII//TRANSLIT"), "/", Sys.Date()) 
     if(!dir.exists(directorio)) { 
       dir.create(directorio, recursive = TRUE)
     }
     inFile  <- input$myFile   
     inFile2 <- input$myFile2
     inFile3 <- input$myFile3
     inFileND <- input$myFileND
     output$contents <- renderTable(input$myFile)
     file.copy(inFile$datapath,
              file.path(
                paste0(directorio, "/", inFile$name) ))
    file.copy(inFile2$datapath,
              file.path(
                paste0(directorio, "/", inFile2$name) ))
    file.copy(inFile3$datapath,
              file.path(
                paste0(directorio, "/", inFile3$name) ))
    file.copy(inFileND$datapath,
              file.path(
                paste0(directorio, "/ND_", inFileND$name) ))
    write.csv(
      x = data.frame(actividad = input$descripcion, 
                     region = paste( input$state, collapse = ", "),
                     vehiculo = paste(input$nave, collapse = ", "),
                     rubro = input$rubro),
      file = paste0(directorio, "/",  "narrativa.txt") , 
      row.names = FALSE
    )
    output$otroscontroles = renderUI({tags$b(HTML('<p style="background-color:MediumSeaGreen;">Comprobación subida</p>' ))})
  })
})
