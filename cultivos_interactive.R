library(dplyr)
library(ggplot2)
library(foreach)
library(readr)
library(plotly)
library(tidyr)
# descarga de:
# http://infosiap.siap.gob.mx/gobmx/datosAbiertos_a.php


prona = png::readPNG("/home/datamarindo/oh_my_gits/miavis/www/INECOL.png")
archivos = list.files("/media/datamarindo/monitoreo/backup2022_reciente/2022-01-02-0005-backup/oficiales", 
                      pattern = ".csv", full.names = T)
produccion = lapply(archivos, 
                    function(x) {read.csv(x) |> janitor::clean_names()})
produccion = do.call(rbind, produccion)

# en Veracruz hay 133 cultivos


q = produccion %>% filter(nomestado == "Veracruz") %>%
  group_by(anio,  nomcultivo, nomddr) %>% 
  summarise(total_toneladas = sum(volumenproduccion, na.rm = T))

# data frame tiene que estar llena de 0,0,0,0,0,0,0,0: no puede haber vacíos, si no se desordena
# TIENE QUE ESTAR ORDENADO POR LA NOMDDR, SI NO NO JALA!!!  me tomó un rato!!!
q = q %>% pivot_wider(names_from = nomddr, values_from = total_toneladas, values_fill = 0) |> 
  pivot_longer(cols = 3:14,  values_to = "total_toneladas", names_to = "nomddr") |> arrange(nomddr)

#los100 = q |> group_by(nomcultivo) |> summarise(maximo = max(total_toneladas)) |>
#  slice_max(maximo, n = 100) |> magrittr::use_series(nomcultivo)
#q = q |> filter(nomcultivo %in% los100 & anio > 2008)



pq = q |>  plot_ly(x = ~anio, y = ~total_toneladas, color = ~nomddr,
             transforms = list(list(type = "filter", 
                                    target = ~nomcultivo,
                                    operation = "="))) %>% 
  layout(title = "Cultivos por Municipio",
         images = list(source = raster2uri(as.raster(prona)),
                       x = 0, y = 1, 
                       sizex = .2, sizey = .2,
                       xref = "paper", yref = "paper",
                       opacity = 0.4),
         xaxis = list(title = "Año"), yaxis = list(title = "Total toneladas"),
         annotations = 
           list(x = 1, y = 0, 
                text = "Fuente: Sistema de Información Agroalimentaria y Pesquera", 
                showarrow = F, xref='paper', yref='paper', 
                xanchor='right', yanchor='auto', xshift=0, yshift=0,
                font=list(size=15)),
         updatemenus = list(
           list(type = "dropdown",
                active=0,
                buttons = apply(as.data.frame(sort(unique(q$nomcultivo))), 1,
                                function(x) list(method = "restyle", 
                                                 args = list("transforms[0].value", x), label = x)))))

add_paths(pq)


ggplotly(ke,transforms = list(list(type = "filter", target = ~nomcultivo,
                                                 operation = "="))) 

